echo stopping containers...
docker stop `docker ps -a | grep friendlyhello | cut -d ' ' -f1`
echo deleting images...
docker container rm `docker ps -a | grep friendlyhello | cut -d ' ' -f1`
docker build -t friendlyhello .
docker run -d -p 4000:80 friendlyhello

